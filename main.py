from google.appengine.ext.webapp import template
from google.appengine.ext import webapp
from google.appengine.api import users
from google.appengine.ext import db
from google.appengine.api import mail

import wsgiref.handlers
import logging

from models import *
from controllers import *
from controllers import pages
from controllers import media
from controllers import articles
from controllers import guests

class MainPage(webapp.RequestHandler):
	method = "GET"
	def get(self, controller, action, var, var2):
		self.method = "GET"
		self.action(controller, action, var, var2)
	def post(self, controller, action, var, var2):
		self.method = "POST"
		self.action(controller, action, var, var2)
		
	def action(self, controller, action, var, var2):
		is_admin = False
		
		if controller == "admin":
			controller = action
			action = var
			var = var2	
			is_admin = True
			
		if controller == "users" or controller == "guests":
			import controllers
			controller = controllers.guests.GuestsController()
		elif controller == "articles":
			controller = articles.ArticlesController()
		elif controller == "media":
			controller = media.MediaController()
		else: 
			#If no Controller is supplied, use the parent Controller
			controller = pages.PagesController()
		
		if action == "":
			action = "home"
		if is_admin:
			action = "admin_" + action
		actionFunc = getattr( controller, action )
		
		if var == "":
			actionFunc(self)
		else:
			actionFunc(self, var)
	
def main():
	try:
		application = webapp.WSGIApplication([
			('/([^/]*)/?([^/]*)/?([^/]*)/?([^/]*)/?', MainPage),
			], debug=True)
		wsgiref.handlers.CGIHandler().run(application)
	except Exception, e:
		raise Exception( "THERE WAS A PROBLEM" + str ( e ) )
		logging.error ( e )

if __name__ == '__main__':
	main()