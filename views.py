import os
import urllib

from google.appengine.api import users
from google.appengine.ext import ndb

from models import *
from admin import *

import jinja2
import webapp2
import json


JINJA_ENVIRONMENT = jinja2.Environment(
	loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
	extensions=['jinja2.ext.autoescape'],
	autoescape=True)
# [END imports]

# [START main_page]
class MainPage(webapp2.RequestHandler):
	def get(self):
		if not User.has_permission(self, 1):
			self.redirect("/login")
		
		template_values = {}
		
		#videos = []
		
		videos = Media.query().filter(Media.type==1).order(Media.date)
		articles = Article.query().order(Article.created).fetch()[0:3]
		tags = Tag.query().order(Tag.name)
		
		#chars = Character.query().fetch()
		
		#for v in temps:
		#	if True:
		#		videos.append(v)

					
		template_values["videos"] = videos
		template_values["articles"] = articles
		template_values["tags"] = tags
		
		template = JINJA_ENVIRONMENT.get_template('templates/index.html')
		self.response.write(template.render(template_values))
		
class LoginPage(webapp2.RequestHandler):
	def get(self):
		if User.has_permission(self,1):
			self.redirect("/")
		else:
			template_values = {}
			template = JINJA_ENVIRONMENT.get_template('templates/admin/login.html')
			self.response.write(template.render(template_values))
		
	def post(self):
		user = User.login(
			self.request.get("name", "guest"),
			self.request.get("password")
		)
		if user:
			self.response.set_cookie("session", user.session)
			template_values = {}
			template_values["thankyou_login"] = True
			template = JINJA_ENVIRONMENT.get_template('templates/admin/login.html')
			self.response.write(template.render(template_values))
		else:
			template_values = {}
			template_values["error_invalid_password"] = True
			template = JINJA_ENVIRONMENT.get_template('templates/admin/login.html')
			self.response.write(template.render(template_values))
			
class LogoutPage(webapp2.RequestHandler):
	def get(self):
		User.logout(self)
		self.response.set_cookie("session","")
		self.redirect("/login")

class MediaQuery(webapp2.RequestHandler):
	def post(self):
		output = {}
		
		if self.request.get("tags", False):
			output["media"] = []
			media = TagMedia.fetch_media(self.request.get("tags").lower().split())
			for m in media:
				output["media"].append({"date":m.get_date("%Y-%m-%d"),"youtube_id":m.youtube_id})
			
				
		
		self.response.out.write(json.dumps(output))

# [START app]
app = webapp2.WSGIApplication([
	('/', MainPage),
	('/query/?', MediaQuery),
	('/login/?', LoginPage),
	('/logout/?', LogoutPage),
	
	('/admin/?', AdminMainPage),
	
	('/admin/articles/?', AdminArticlesIndex),
	('/admin/articles/add/?', AdminArticlesAdd),
	('/admin/articles/edit/(\d+)/?', AdminArticlesEdit),
	
	('/admin/media/?', AdminMediaIndex),
	('/admin/media/add/?', AdminMediaAdd),
	('/admin/media/edit/(\d+)/?', AdminMediaEdit),
], debug=True)
# [END app]
