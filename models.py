from datetime import datetime
from google.appengine.ext import ndb
from password import password_scramble
import random
import re

class AppModel( ndb.Model ):	
	created = ndb.DateTimeProperty(auto_now_add=True)
	modified = ndb.DateTimeProperty(auto_now=True)
	
	def get_created(self, format):
		return self.created.strftime(format)
		
	#class Meta:
	#	abstract = True
	
class Media( AppModel ):
	type = ndb.IntegerProperty(default=1)	
	date = ndb.DateTimeProperty(auto_now_add=True)
	
	youtube_id = ndb.StringProperty()
	image_url = ndb.StringProperty()
	access = ndb.IntegerProperty(default=1)
	
	user = ndb.UserProperty(auto_current_user_add=True)
	
	def get_date(self, format):
		return self.date.strftime(format)
	
	def getTags( self ):
		tms = TagMedia.query().filter(TagMedia.media==self.key).fetch()
		out = []
		for tm in tms:
			out.append( tm.tag.get() )
		return out
	
	def setTags( self, tags ):
		#delete existing tags
		TagMedia.delete_many(self.key)
		for tag in tags:
			tm = TagMedia()
			tm.media = self.key
			tm.tag = tag.key
			tm.put()
			
	@staticmethod
	def str_to_date(d):
		d = re.match("(\d{4})-(\d{2})-(\d{2})", d)
		if d:
			return datetime(int(d.group(1)),month=int(d.group(2)),day=int(d.group(3)))
		return datetime.now()
	
class Article( AppModel ):
	type = ndb.IntegerProperty()	
	
	title = ndb.StringProperty()
	image = ndb.StringProperty()
	text = ndb.StringProperty()
	playlist = ndb.TextProperty()
	
	user = ndb.UserProperty(auto_current_user_add=True)
	
class Tag( AppModel ):
	name =  ndb.StringProperty()
	type = ndb.IntegerProperty(default=1)
	#1 = person, 2 = place, 3 = event
	
	@staticmethod
	def fetch_create(name):
		out = False
		tag = Tag.query().filter(Tag.name==name).get()
		
		if tag:
			return tag
		
		tag = Tag(name=name)
		tag.put()
		return tag
	
	@staticmethod
	def list(tags):
		tags = tags.lower().split(',')
		out = []
		for t in tags:
			out.append( Tag.fetch_create( t.strip() ) )
		return out
		
class TagMedia( AppModel ):
	tag = ndb.KeyProperty(kind=Tag)
	media = ndb.KeyProperty(kind=Media)
	
	@staticmethod
	def delete_many(media_key):
		tms = TagMedia.query().filter(TagMedia.media==media_key).fetch()
		for tm in tms:
			tm.key.delete()
	
	@staticmethod
	def fetch_media(tagnames):
		out = []
		media_keys = []
		for tagname in tagnames:
			tag = Tag.query().filter(Tag.name==tagname).get()
			tms = TagMedia.query().filter(TagMedia.tag==tag.key).fetch()
			for tm in tms:
				if not tm.media in media_keys:
					media_keys.append(tm.media)
		for media_key in media_keys:
			out.append(media_key.get())
		return out
	
class Config( AppModel ):
	name = ndb.StringProperty()
	value = ndb.StringProperty()
	
class User( AppModel ):
	name = ndb.StringProperty()
	password = ndb.StringProperty()
	session = ndb.StringProperty()
	level = ndb.IntegerProperty(default=1)
	
	@staticmethod
	def login(username, password):
		user = False
		#import pdb; pdb.set_trace()
		password = password_scramble(password)
		
		if username == "guest" or username == "admin":
			user = User.query().filter(User.name==username).get()
			if not user:
				user = User(name="guest",password=password)
				user.put()
		
		user = User.query().filter(User.name==username).filter(User.password==password).get()
			
		if user:
			user.session = User.randomSession(16)
			user.put()
		
		return user
	
	@staticmethod
	def logout(request):
		session = request.request.cookies.get("session")
		user = User.query().filter(User.session==session).get()
		if user:
			user.session = None
			user.put()
	
	@staticmethod
	def randomSession(size):
		out = ""
		for i in range(size):
			out += str(int(random.random()*10))
		return out
		
	@staticmethod
	def has_permission(request, level):
		session = request.request.cookies.get("session")
		user = User.query().filter(User.session==session).get()
		if user:
			return user.level >= level
		return False
	
	@staticmethod
	def get(user):
		out = False
		users = User.all().filter('name',user)
		for user in users:
			out = user
		return out