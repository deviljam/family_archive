import os
import urllib

from google.appengine.api import users
from google.appengine.ext import ndb

from models import *

import jinja2
import webapp2
import json


JINJA_ENVIRONMENT = jinja2.Environment(
	loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
	extensions=['jinja2.ext.autoescape'],
	autoescape=True)
# [END imports]

# [START main_page]
class AdminMainPage(webapp2.RequestHandler):
	def get(self):
		if not User.has_permission(self, 2):
			self.redirect("/login")
		
		template_values = {}
		template = JINJA_ENVIRONMENT.get_template('templates/admin/index.html')
		self.response.write(template.render(template_values))
		

#Articles
class AdminArticlesIndex(webapp2.RequestHandler):
	def get(self):
		if not User.has_permission(self, 2):
			self.redirect("/login")
		
		template_values = {}
		articles = Article.query().fetch()
		
		template_values["articles"] = articles
		
		template = JINJA_ENVIRONMENT.get_template('templates/admin/articles/index.html')
		self.response.write(template.render(template_values))

		
class AdminArticlesAdd(webapp2.RequestHandler):
	def get(self):
		if not User.has_permission(self, 2):
			self.redirect("/login")
			
		template_values = {"page":"add"}
		template_values["article"] = {}
		
		template = JINJA_ENVIRONMENT.get_template('templates/admin/articles/add.html')
		self.response.write(template.render(template_values))
		
	def post(self):
		if not User.has_permission(self, 2):
			self.redirect("/login")
			
		art = Article()
		art.type = int(self.request.get("type","0"))
		art.title = self.request.get("title","noname")
		art.image = self.request.get("image")
		art.text = self.request.get("text")
		art.playlist = self.request.get("playlist")
		art.put()
		
		self.redirect("/admin/articles")
		
class AdminArticlesEdit(webapp2.RequestHandler):
	def get(self,id):
		if not User.has_permission(self, 2):
			self.redirect("/login")
			
		template_values = {"page":"edit"}
		template_values["id"] = id
		template_values["article"] = Article.get_by_id(int(id))
		
		template = JINJA_ENVIRONMENT.get_template('templates/admin/articles/add.html')
		self.response.write(template.render(template_values))
		
	def post(self,id):
		if not User.has_permission(self, 2):
			self.redirect("/login")
			
		art = Article.get_by_id(int(id))
		art.type = int(self.request.get("type","0"))
		art.title = self.request.get("title","noname")
		art.image = self.request.get("image")
		art.text = self.request.get("text")
		art.playlist = self.request.get("playlist")
		art.put()
		
		self.redirect("/admin/articles")
		

#Media
class AdminMediaIndex(webapp2.RequestHandler):
	def get(self):
		if not User.has_permission(self, 2):
			self.redirect("/login")
			
		template_values = {}
		media = Media.query().fetch()
		
		template_values["media_list"] = media
		
		template = JINJA_ENVIRONMENT.get_template('templates/admin/media/index.html')
		self.response.write(template.render(template_values))

		
class AdminMediaAdd(webapp2.RequestHandler):
	def get(self):
		if not User.has_permission(self, 2):
			self.redirect("/login")
			
		template_values = {"page":"add"}
		template_values["media"] = {}
		template_values["tags"] = Tag.query().fetch()
		
		template = JINJA_ENVIRONMENT.get_template('templates/admin/media/add.html')
		self.response.write(template.render(template_values))
		
	def post(self):
		if not User.has_permission(self, 2):
			self.redirect("/login")
			
		media = Media()
		media.type = int(self.request.get("type","0"))
		media.youtube_id = self.request.get("youtube_id")
		media.image_url = self.request.get("image_url")
		media.date = Media.str_to_date(self.request.get("date"))
		media.access = int(self.request.get("access","0"))
		media.setTags( Tag.list( self.request.get('tags') ) )
		media.put()
		
		self.redirect("/admin/media")
		
class AdminMediaEdit(webapp2.RequestHandler):
	def get(self,id):
		if not User.has_permission(self, 2):
			self.redirect("/login")
			
		template_values = {"page":"edit"}
		template_values["id"] = id
		template_values["media"] = Media.get_by_id(int(id))
		template_values["taglist"] = template_values["media"].getTags()
		template_values["tags"] = Tag.query().fetch()
		
		template = JINJA_ENVIRONMENT.get_template('templates/admin/media/add.html')
		self.response.write(template.render(template_values))
		
	def post(self,id):
		if not User.has_permission(self, 2):
			self.redirect("/login")
			
		media = Media.get_by_id(int(id))
		media.type = int(self.request.get("type","0"))
		media.youtube_id = self.request.get("youtube_id")
		media.image_url = self.request.get("image_url")
		media.date = Media.str_to_date(self.request.get("date"))
		media.access = int(self.request.get("access", "0"))
		media.setTags( Tag.list( self.request.get('tags') ) )
		media.put()
		
		self.redirect("/admin/media")