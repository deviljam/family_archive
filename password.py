import string
import md5

def password_scramble(password, salt="a2345fdmf",size=16):
	m = md5.new()
	m.update(salt)
	m.update(password)
	return str(m.hexdigest())
	