var timeline;
var player;
$(document).ready(function(){
	$('.timeline').each( function(){
		timeline = new Timeline( this );
		timeline.addVideos( init_data.videos );
	});
	$('.timeline .controls .prev').click(function(e){timeline.moveBy( 500 );});
	$('.timeline .controls .next').click(function(e){timeline.moveBy( -500 );});
	$('.timeline .controls .in').click(function(e){timeline.zoomIn();});
	$('.timeline .controls .out').click(function(e){timeline.zoomOut();});
	
	$('#timeline .header').click(function(e){timeline.toggleOpen();});
	
	$('.video_thumb').click(function(e){
		e.preventDefault();
		var youtube_id = $(this).data('video');
		player.setPlaylist(youtube_id);
	});
	
	player = new Player();
	
	FooterSearch.init();
	
	$('article a.play, article .thumb').click(function(e){
		e.preventDefault();
		if ( player instanceof Player ) {
			player.open();
			
			DataManager.query({
				data : $(this).data('query'),
				success:function(res){
					var list = new Array();
					for ( var i=0;i<res.media.length;i++ ) {
						list.push( res.media[i].youtube_id );
					}
					if ( list.length > 0 ){
						player.setPlaylist(list);
					}
				}
			});
		}
	});
});

function onYouTubeIframeAPIReady(){
	//player = new Player();
}

function Timeline( element ){
	this.element = element;
	this.init_pos = Math.floor( $(this.element).css('top').replace('px','') );
	this.scale = 0.25;
	this.offset = -10800;
	this.baseyear = 1950;
	
	this._open = true;
	
	this.videos = [];
	this.headers = [];
	
	this.elm_pane = document.createElement('div');
	this.elm_markers = document.createElement('div');
	this.elm_headers = document.createElement('div');
	this.elm_pane.className = "pane";
	this.elm_markers.className = "markers";
	this.elm_headers.className = "headers";
	this.elm_pane.appendChild(this.elm_headers);
	this.elm_pane.appendChild(this.elm_markers);
	this.element.appendChild(this.elm_pane);
		
	var today = new Date().getFullYear();
	for(var year=this.baseyear; year<=today; year++ ) {
		this.createYearHeader(year);
	}
	
	this.close();
}

Timeline.prototype.createYearHeader = function( year ){
	var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
	
	var container = document.createElement('div');
	var title = document.createElement('div');
	
	container.className = "year";
	title.className = "title";
	title.innerHTML = year;
	container.appendChild( title ); 
	
	container.year = year;
	container.onclick = function(){
		var start = Math.floor( this.year );
		var end = year + 1;
		DataManager.query({
			data:'{startdate:'+start+'-01-01, enddate:'+end+'-01-01}',
			success:function(res){
				var list = new Array();
				for ( var i in res.media ) {
					list.push( res.media[i].youtube_id );
				}
				if ( list.length > 0 ){
					player.setPlaylist(list);
				}
			}
		});
	}
	
	for( var i=0;i<months.length;i++){
		var month = document.createElement('div');
		month.className = "month " + months[i].toLowerCase();
		month.innerHTML = months[i];
		container.appendChild( month ); 
	}
	
	var start = new Date(this.baseyear,01,01);
	var date = new Date(year,01,01);
	var position = (date - start) / (1000*60*60*24);
	
	var header = {
		year : year,
		position: position,
		element : container
	};
	
	this.elm_headers.appendChild( container );
	this.headers.push( header );
	this.update();
}

Timeline.prototype.toggleOpen = function(){
	if ( this._open ) { 
		this.close(); 
	} else {
		this.open();
	}
}
Timeline.prototype.open = function(){
	this.element.style.height = "";
	this.elm_pane.style.display = "block";
	$('.timeline .controls').show();
	this._open = true;
}
Timeline.prototype.close = function(){
	this.element.style.height = "40px";
	this.elm_pane.style.display = "none";
	$('.timeline .controls').hide();
	this._open = false;
}



Timeline.prototype.addVideos = function( videos ){
	for ( var i=0; i<videos.length;i++){
		var start = new Date(this.baseyear,01,01);
		var date = new Date(videos[i].date);
		var position = (date - start) / (1000*60*60*24);
		video = {
			timeline:this,
			element:document.createElement('div'),
			position:position,
			data:videos[i]
		};
		video.element.className = "video";
		video.element.videoData = video;
		video.element.onclick = function(){ player.play( this.videoData.data.youtube_id );};
		
		this.elm_markers.appendChild( video.element );
		this.videos.push ( video );
	}
	this.update();
}

Timeline.prototype.moveBy = function( amount, absolute ){
	if ( !absolute ){
		amount = amount / this.scale;
	}
	this.offset += amount;
	var to = (this.offset * this.scale );
	$(this.elm_pane).animate({'left':to });
}
Timeline.prototype.zoomIn = function(){ this.scale *= 2; this.update(); }
Timeline.prototype.zoomOut = function(){ this.scale /= 2; this.update(); }

Timeline.prototype.update = function( ){
	this.elm_pane.style.left = (this.offset * this.scale ) + "px";
	for( var i=0; i<this.videos.length;i++){
		var video = this.videos[i];
		video.element.style.left = (video.position * this.scale) + "px";
	}
	
	for( var i=0; i<this.headers.length;i++){
		var header = this.headers[i];
		header.element.style.left = (header.position * this.scale) + "px";
		header.element.style.width = (365.242 * this.scale) + "px";
	}
	
	if (  this.scale >= 2 ) {
		$(this.elm_headers).removeClass('small');
	}else{
		$(this.elm_headers).addClass('small');
	}
}


// Player

function Player( ){
	this.list = [];
	this.current;
	this.element = document.getElementById('player');
	this.yt;
	
	if ( this.element == undefined ){
		this.element = document.createElement( 'div' );
		this.element.id = 'player';
		this.element.style.display = 'none';
		
		var player_element = document.createElement( 'div' );
		var player_next = document.createElement( 'a' );
		var player_prev = document.createElement( 'a' );
		var player_close = document.createElement( 'a' );
		var player_buts = document.createElement( 'div' );
		
		player_element.id = 'player_element';
		player_prev.className = "btn btn-primary prev";
		player_next.className = "btn btn-primary next";
		player_close.className = "btn btn-primary close";
		player_buts.className = "buttons";
		
		player_prev.innerHTML = "&lt;";
		player_next.innerHTML = "&gt;";
		player_close.innerHTML = "Stop";
		player_prev.onclick = function(){ player.prev(); }
		player_next.onclick = function(){ player.next(); }
		player_close.onclick = function(){ player.close(); }
		
		player_buts.appendChild(player_prev);
		player_buts.appendChild(player_close);
		player_buts.appendChild(player_next);
		this.element.appendChild(player_element);
		this.element.appendChild(player_buts);
		document.body.appendChild(this.element);
	}
	
	//this.setPlaylist('nzqF_gBpS84,oIIxlgcuQRU');
}

Player.prototype.setPlaylist = function( list ){
	if( list instanceof Array ) {
		this.list = list;
	} else {
		this.list = list.split(",");
	}
	this.current = 0;
	
	this.play( this.list[0] );
}

Player.prototype.play = function(id){
	var youtube_player = this;
	this.open();
	
	if ( this.yt == undefined ) {
		this.yt = new YT.Player('player_element', {
			height: '390',
			width: '640',
			videoId: id,
			events: {
				'onStateChange': function(event){ youtube_player.stateChanged(event); }
			}
		});
	}else {
		player.yt.loadVideoById( id );
	}
	
	if ( this.current > 0 ){
		$('#player .prev').removeAttr('disabled')
	}else {
		$('#player .prev').attr('disabled','disabled')
	}
	
	if ( this.current+1 < this.list.length ){
		$('#player .next').removeAttr('disabled')
	}else {
		$('#player .next').attr('disabled','disabled')
	}
}

Player.prototype.open = function( list ){
	this.element.style.display = 'block';
}
Player.prototype.close = function(){
	this.element.style.display = "none";
	this.yt.pauseVideo();
}

Player.prototype.prev = function(){
	if ( this.current > 0 ) {
		this.current--;
		this.play( this.list[this.current] );
	}
}

Player.prototype.next = function(){
	if ( (this.current + 1) < this.list.length ) {
		this.current++;
		this.play( this.list[this.current] );
	}
}

Player.prototype.stateChanged = function(event){
	//console.log(event.data);
	if ( event.data == -1 ) {
		this.yt.playVideo();
	}
	if ( event.data == 0 ) {
		//video has ended, play the next one.
		this.current++;
		if ( this.current < this.list.length ) {
			this.play( this.list[this.current] );
		} else {
			this.close();
		}
	}
}


Query = {
	"make" : function(query){
		//Open Player
		if ( player instanceof Player ) {
			player.open();
			
			//Make API call
			DataManager.query({
				data : query,
				success:function(res){
					var list = new Array();
					for ( var i=0;i<res.media.length;i++ ) {
						list.push( res.media[i].youtube_id );
					}
					if ( list.length > 0 ){
						player.setPlaylist(list);
					}
				}
			});
		}
	}
}
String.prototype.clean = function(){return this.replace(/(?:(?:^|\n)\s+|\s+(?:$|\n))/g,'').replace(/\s+/g,' ');};