function queryToObject( str ){
	out = {};
	var tags = str.match(/tags\s*:\s*\[(.+)\]/);
	var start = str.match(/startdate\s*:\s*(\d\d\d\d-\d\d-\d\d)/);
	var end = str.match(/enddate\s*:\s*(\d\d\d\d-\d\d-\d\d)/);
	
	if ( tags instanceof Array ){ out['tags'] = tags[1]; }
	if ( start instanceof Array ){ out['startdate'] = start[1]; }
	if ( end instanceof Array ){ out['enddate'] = end[1]; }
	return out;
}

DataManager ={
	query : function(params){
		params = params || {};
		success = params['success'] || null;
		error = params['error'] || null;
		context = params['context'] || window;
		data = params['data'] || '';
		data = queryToObject( data );
		$.ajax({
			url:'/query',
			type:'POST',
			data:data,
			dataType:'json',
			success:success,
			error:error,
			context:context
		});
	}
}
