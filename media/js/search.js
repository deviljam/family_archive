var FooterSearch = {
	"init" : function(){
		var self = this;
		this.search_input = $('#tag_search .input-prepend input.search');
		this.search_select = $('#tag_search .tag_list');
		this.search_selected = $('#tag_search .tag_selected');
		this.search_button = $('#js_search_tags');
		
		//Show/hide search help
		this.search_input.on('focus', function(){
			self.search_select.show();
		});
		this.search_input.on('blur', function(){
			setTimeout(function(){
				self.search_select.hide();
			}, 300 );
		});
		this.search_input.on('keyup', function(){
			self.text_change();
		});
		this.search_select.find('li').click(function(){
			var tag_name = $(this).data('tag');
			var tag = self.search_selected.find('[data='+tag_name+']');
			if( tag.length < 1 ){
				var new_tag = $(
					"<li data-tag='"+ tag_name +"'>"+
						tag_name + 
					"</li>"
				);
				self.search_selected.append( new_tag );
				
				$(new_tag).click(function(event){
					event.preventDefault;
					self.removeSelectedTag($(this).data('tag'));
				});
			}
		});
		this.search_button.click(function(ev){
			ev.preventDefault();
			self.search();
		});
	},
	
	"text_change" : function(){
		var text = this.search_input.val();
		
		tags = this.search_select.find('li');
		for(var i=0; i<tags.length;i++){
			var $tag = $(tags[i]);
			if( $tag.data('tag').match("^" + text) ){
				$tag.show();
			} else { 
				$tag.hide();
			}
		}
	},
	
	"removeSelectedTag" : function(name){
		this.search_selected.find("[data-tag="+name+"]").remove();
	},
	 
	"search" : function(){
		var tags = this.search_selected.find('li');
		var url = "/media/query";
		var data = "";
		
		for(var i=0;i<tags.length;i++){
			var tag = tags[i];
			data += data.length > 0 ? "," : "";
			data += $(tag).data('tag');
		}
		
		Query.make( "tags:["+data+"]" );
	}

}